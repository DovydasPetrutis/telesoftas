﻿using System.Collections.Generic;
using ConsoleApp;
using ConsoleApp.Data;
using ConsoleApp.Data.Constants;
using ConsoleApp.Data.Enums;
using ConsoleApp.Processors;
using FluentAssertions;
using Xunit;

namespace ApplicationTests
{
	public class ItemProcessorTests
	{
		// Check if items are updated as expected when passing various data
		[Theory]
		[MemberData(nameof(TestData))]
		public void ProcessItemShouldUpdateAsExpected(string name, int sellIn, int quality, int newSellIn, int newQuantity)
		{
			var item = new Item { Name = name, Quality = quality, SellIn = sellIn };
			ItemProcessor.ItemProcessorInstance(item).ProcessItem(item);
			item.Quality.Should().Be(newQuantity);
			item.SellIn.Should().Be(newSellIn);
		}

		public static IEnumerable<object[]> TestData =>
			new List<object[]>{
				new object[] { NamesConstants.VEST, 10, 20, 9, 19},
				new object[] { NamesConstants.VEST, 10, 0, 9, 0},
				new object[] { NamesConstants.VEST, 10, 20, 9, 19},
				new object[] { NamesConstants.BRIE, 2, 0, 1, 1},
				new object[] { NamesConstants.BRIE, 0, 0, -1, 2},
				new object[] { NamesConstants.ELIXIR, 5, 7, 4, 6},
				new object[] { NamesConstants.SULFURAS, 0, 80, 0, 80},
				new object[] { NamesConstants.SULFURAS, -1, 80, -1, 80},
				new object[] { NamesConstants.PASSES, 15, 20, 14, 21},
				new object[] { NamesConstants.PASSES, 10, 20, 9, 22},
				new object[] { NamesConstants.PASSES, 5, 20, 4, 23},
				new object[] { NamesConstants.PASSES, 0, 20, -1, 0},
				new object[] { NamesConstants.CONJURED, 3, 6, 2, 4},
				new object[] { NamesConstants.CONJURED, 10, 20, 9, 18},
				new object[] { NamesConstants.CONJURED, 10, 0, 9, 0},
				new object[] { NamesConstants.CONJURED, 0, 20, -1, 16},
			};

		// Tests item category
		[Fact]
		public void CheckItemCategories()
		{
			ItemProcessor.GetItemCategory(new Item { Name = "My new item" }).Should()
				.Be(ItemCategory.NormalItem);

			ItemProcessor.GetItemCategory(new Item { Name = NamesConstants.SULFURAS }).Should()
				.Be(ItemCategory.Legendary);

			ItemProcessor.GetItemCategory(new Item { Name = NamesConstants.CONJURED }).Should()
				.Be(ItemCategory.ConjuredItem);

			ItemProcessor.GetItemCategory(new Item { Name = NamesConstants.PASSES }).Should()
				.Be(ItemCategory.BackstagePasses);

			ItemProcessor.GetItemCategory(new Item { Name = NamesConstants.BRIE }).Should()
				.Be(ItemCategory.QualityDegradesTwice);

			ItemProcessor.GetItemCategory(new Item { Name = NamesConstants.VEST }).Should()
				.Be(ItemCategory.NormalItem);

			ItemProcessor.GetItemCategory(new Item { Name = NamesConstants.ELIXIR }).Should()
				.Be(ItemCategory.NormalItem);

		}

		// Test if item processor is of the right type
		[Fact]
		public void CreateProcessorByCategory()
		{
			ItemProcessor.ItemProcessorInstance(new Item {Name = NamesConstants.BRIE}).Should().BeOfType<DegradesTwiceProcessor>();
			ItemProcessor.ItemProcessorInstance(new Item {Name = NamesConstants.PASSES}).Should().BeOfType<BackstageProcessor>();
			ItemProcessor.ItemProcessorInstance(new Item {Name = NamesConstants.SULFURAS}).Should().BeOfType<LegendaryProcessor>();
			ItemProcessor.ItemProcessorInstance(new Item {Name = NamesConstants.CONJURED}).Should().BeOfType<ConjuredProcessor>();
			ItemProcessor.ItemProcessorInstance(new Item {Name = "My another new item"}).Should().BeOfType<NormalProcessor>();
		}


		// Test every item processor if its calculations are rigth
		[Theory]
		[MemberData(nameof(TestData))]
		public void DegradesTwiceProcessorShouldProcessOnlyQualityDegradesTwiceCategory(string name, int sellIn, int quality, int newSellIn, int newQuantity)
		{
			var item = new Item{Name = name};

			var processor = ItemProcessor.ItemProcessorInstance(item);
			if (ItemProcessor.GetItemCategory(item) == ItemCategory.QualityDegradesTwice)
				processor.Should().BeOfType<DegradesTwiceProcessor>();
		}

		[Theory]
		[MemberData(nameof(TestData))]
		public void BackstageProcessorShouldProcessOnlyQualityDegradesTwiceCategory(string name, int sellIn, int quality, int newSellIn, int newQuantity)
		{
			var item = new Item { Name = name };

			var processor = ItemProcessor.ItemProcessorInstance(item);
			if (ItemProcessor.GetItemCategory(item) == ItemCategory.BackstagePasses)
				processor.Should().BeOfType<BackstageProcessor>();
		}

		[Theory]
		[MemberData(nameof(TestData))]
		public void ConjuredProcessorShouldProcessOnlyQualityDegradesTwiceCategory(string name, int sellIn, int quality, int newSellIn, int newQuantity)
		{
			var item = new Item { Name = name };

			var processor = ItemProcessor.ItemProcessorInstance(item);
			if (ItemProcessor.GetItemCategory(item) == ItemCategory.ConjuredItem)
				processor.Should().BeOfType<ConjuredProcessor>();
		}

		[Theory]
		[MemberData(nameof(TestData))]
		public void LegendaryProcessorShouldProcessOnlyQualityDegradesTwiceCategory(string name, int sellIn, int quality, int newSellIn, int newQuantity)
		{
			var item = new Item { Name = name };

			var processor = ItemProcessor.ItemProcessorInstance(item);
			if (ItemProcessor.GetItemCategory(item) == ItemCategory.Legendary)
				processor.Should().BeOfType<LegendaryProcessor>();
		}

		[Theory]
		[MemberData(nameof(TestData))]
		public void NormalProcessorShouldProcessOnlyQualityDegradesTwiceCategory(string name, int sellIn, int quality, int newSellIn, int newQuantity)
		{
			var item = new Item { Name = name };

			var processor = ItemProcessor.ItemProcessorInstance(item);
			if (ItemProcessor.GetItemCategory(item) == ItemCategory.NormalItem)
				processor.Should().BeOfType<NormalProcessor>();
		}
	}
}