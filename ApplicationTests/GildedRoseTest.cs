﻿using Xunit;
using System.Collections.Generic;
using ConsoleApp;
using ConsoleApp.Data;
using FluentAssertions;

namespace ApplicationTests
{
    public class GildedRoseTest
    {
        [Fact]
        public void ProgramRunsAsPrescribed()
        {

            IList<Item> items = new List<Item>{
                new Item {Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20},
                new Item {Name = "Aged Brie", SellIn = 2, Quality = 0},
                new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7},
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80},
                new Item {Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 15, Quality = 20},
                new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6}
            };
            GildedRose g = new GildedRose(items);

            g.UpdateQuality();

            // Check names not changed
            items[0].Name.Should().Be("+5 Dexterity Vest");
            items[1].Name.Should().Be("Aged Brie");
            items[2].Name.Should().Be("Elixir of the Mongoose");
            items[3].Name.Should().Be("Sulfuras, Hand of Ragnaros");
            items[4].Name.Should().Be("Backstage passes to a TAFKAL80ETC concert");
            items[5].Name.Should().Be("Conjured Mana Cake");

            // Check SellIn changed as expected
            items[0].SellIn.Should().Be(9);
            items[1].SellIn.Should().Be(1);
            items[2].SellIn.Should().Be(4);
            items[3].SellIn.Should().Be(0);
            items[4].SellIn.Should().Be(14);
            items[5].SellIn.Should().Be(2);

            // Check Quality changed as expected
            items[0].Quality.Should().Be(19);
            items[1].Quality.Should().Be(1);
            items[2].Quality.Should().Be(6);
            items[3].Quality.Should().Be(80);
            items[4].Quality.Should().Be(21);
            items[5].Quality.Should().Be(4);
        }
    }
}