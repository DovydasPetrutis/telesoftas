﻿using ConsoleApp.Data;

namespace ConsoleApp.Processors
{
	public class NormalProcessor : ItemProcessor
	{
		public override void ProcessItem(Item item)
		{
			if (--item.SellIn < 0)
				item.Quality -= 2;
			else
				item.Quality -= 1;

			if (item.Quality < 0)
				item.Quality = 0;
		}
	}
}