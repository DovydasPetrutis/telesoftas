﻿using ConsoleApp.Data;

namespace ConsoleApp.Processors
{
	public class ConjuredProcessor : ItemProcessor
	{
		public override void ProcessItem(Item item)
		{
			if (--item.SellIn < 0)
				item.Quality -= 4;
			else
				item.Quality -= 2;

			if (item.Quality < 0)
				item.Quality = 0;
		}
	}
}