﻿using ConsoleApp.Data;
using ConsoleApp.Data.Constants;
using ConsoleApp.Data.Enums;

namespace ConsoleApp.Processors
{
	public class ItemProcessor
	{
		public static ItemProcessor ItemProcessorInstance(Item item)
		{
			switch (GetItemCategory(item))
			{
				case ItemCategory.QualityDegradesTwice:
					return new DegradesTwiceProcessor();
				case ItemCategory.BackstagePasses:
					return new BackstageProcessor();
				case ItemCategory.Legendary:
					return new LegendaryProcessor();
				case ItemCategory.ConjuredItem:
					return new ConjuredProcessor();
				case ItemCategory.NormalItem:
					return new NormalProcessor();
				default:
					return new ItemProcessor();
			}
		}

		public virtual void ProcessItem(Item item)
		{
		}

		public static ItemCategory GetItemCategory(Item item)
		{
			// Lines 46-58 could be also be replaces as:
			/*
			  return item.Name switch
				{
					NamesConstants.BRIE => ItemCategory.QualityDegradesTwice,
					NamesConstants.PASSES => ItemCategory.BackstagePasses,
					NamesConstants.SULFURAS => ItemCategory.Legendary,
					NamesConstants.CONJURED => ItemCategory.ConjuredItem,
					_ => ItemCategory.NormalItem
				};
			 */

			switch (item.Name)
			{
				case NamesConstants.BRIE:
					return ItemCategory.QualityDegradesTwice;
				case NamesConstants.PASSES:
					return ItemCategory.BackstagePasses;
				case NamesConstants.SULFURAS:
					return ItemCategory.Legendary;
				case NamesConstants.CONJURED:
					return ItemCategory.ConjuredItem;
				default:
					return ItemCategory.NormalItem;
			}
		}
	}
}
