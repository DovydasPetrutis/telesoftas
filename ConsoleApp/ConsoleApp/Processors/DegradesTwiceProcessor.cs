﻿

using ConsoleApp.Data;

namespace ConsoleApp.Processors
{
	public class DegradesTwiceProcessor : ItemProcessor
	{
		public override void ProcessItem(Item item)
		{
			// Lines 13 - 16 could be also written like this:
			// _ = --item.SellIn < 0 ? item.Quality += 2 : item.Quality++;
			if (--item.SellIn < 0)
				item.Quality += 2;
			else
				item.Quality++;

			if (item.Quality > 50)
				item.Quality = 50;
		}
	}
}