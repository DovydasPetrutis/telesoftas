﻿using System.Collections.Generic;
using ConsoleApp.Processors;
using ConsoleApp.Data;

namespace ConsoleApp
{
	public class GildedRose
	{
		readonly IList<Item> _items;
		public GildedRose(IList<Item> items)
		{
			_items = items;
		}

		public void UpdateQuality()
		{
			foreach (var item in _items)
				ItemProcessor.ItemProcessorInstance(item).ProcessItem(item);
		}
	}
}
