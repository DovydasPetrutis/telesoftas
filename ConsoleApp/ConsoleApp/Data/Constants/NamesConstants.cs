﻿namespace ConsoleApp.Data.Constants
{
	public class NamesConstants
	{
		public const string VEST = "+5 Dexterity Vest";
		public const string BRIE = "Aged Brie";
		public const string ELIXIR = "Elixir of the Mongoose";
		public const string SULFURAS = "Sulfuras, Hand of Ragnaros";
		public const string PASSES = "Backstage passes to a TAFKAL80ETC concert";
		public const string CONJURED = "Conjured Mana Cake";
	}
}
