﻿namespace ConsoleApp.Data.Enums
{
	public enum ItemCategory
	{
		NormalItem,
		QualityDegradesTwice,
		Legendary,
		BackstagePasses,
		ConjuredItem
	}
}
